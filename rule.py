__author__ = 'michi'

import handType


class FightRule:
    def __init__(self):
        pass

    def fight(player1hand, player2hand):
        if player1hand == player2hand:
            return "あいこ"

        if player1hand == handType.HandType.PAPER:
            if player2hand == handType.HandType.SCISSORS:
                return "player2の勝ち"
            if player2hand == handType.HandType.STONE:
                return "player1の勝ち"
        if player1hand == handType.HandType.SCISSORS:
            if player2hand == handType.HandType.STONE:
                return "player2の勝ち"
            if player2hand == handType.HandType.PAPER:
                return "player1の勝ち"
        if player1hand == handType.HandType.STONE:
            if player2hand == handType.HandType.PAPER:
                return "player2の勝ち"
            if player2hand == handType.HandType.SCISSORS:
                return "player1の勝ち"

        return "結果"
