__author__ = 'michi'
from enum import Enum

class HandType(Enum):
    PAPER = 1
    STONE = 2
    SCISSORS = 3