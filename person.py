__author__ = 'michi'

import handType
import random

class Person:
    def __init__(self, name):
        self.name = name

    def choice_hand(self):
        seed = random.randint(0, 2)
        if seed == 0:
            self.hand = handType.HandType.PAPER
        elif seed == 1:
            self.hand = handType.HandType.STONE
        else:
            self.hand = handType.HandType.SCISSORS