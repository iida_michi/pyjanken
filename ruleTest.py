__author__ = 'michi'

import unittest
import rule
import handType

class RuleTestCase(unittest.TestCase):
    def test_PP(self):
        self.assertEqual("あいこ", rule.FightRule.fight(handType.HandType.PAPER,handType.HandType.PAPER))

    def test_PSC(self):
        self.assertEqual("player2の勝ち", rule.FightRule.fight(handType.HandType.PAPER,handType.HandType.SCISSORS))

    def test_PST(self):
        self.assertEqual("player1の勝ち", rule.FightRule.fight(handType.HandType.PAPER,handType.HandType.STONE))

    def test_SCST(self):
        self.assertEqual("player2の勝ち", rule.FightRule.fight(handType.HandType.SCISSORS,handType.HandType.STONE))

    def test_SCP(self):
        self.assertEqual("player1の勝ち", rule.FightRule.fight(handType.HandType.SCISSORS,handType.HandType.PAPER))

    def test_STSC(self):
        self.assertEqual("player1の勝ち", rule.FightRule.fight(handType.HandType.STONE,handType.HandType.SCISSORS))

    def test_STP(self):
        self.assertEqual("player2の勝ち", rule.FightRule.fight(handType.HandType.STONE,handType.HandType.PAPER))

    def test_STST(self):
        self.assertEqual("あいこ", rule.FightRule.fight(handType.HandType.STONE,handType.HandType.STONE))


if __name__ == '__main__':
    unittest.main()
